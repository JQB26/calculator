import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Calculator',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Calculator'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  Widget buildButton(){
    return new Expanded(
      child: new OutlineButton(
        child: Text("1"),
        onPressed: () => {},
      ),
    );
  }

  @override
  Widget build(BuildContext context) {

    return new Scaffold(
      appBar: new AppBar(
        title: new Text(widget.title),
      ),
      body: new Container(
        child: new Column(children: <Widget> [
          new Text("0"),

          new Expanded(
            child: new Divider(),
          ),

          new Column(
            children: [
              new Row(
                children: [
                  buildButton(),
                  buildButton(),
                  buildButton(),
                  buildButton(),
                  buildButton(),
                ],
              ),
              new Row(
                children: [
                  buildButton(),
                  buildButton(),
                  buildButton(),
                  buildButton(),
                  buildButton(),
                ],
              ),
              new Row(
                children: [
                  buildButton(),
                  buildButton(),
                  buildButton(),
                  buildButton(),
                  buildButton(),
                ],
              ),
              new Row(
                children: [
                  buildButton(),
                  buildButton(),
                  buildButton(),
                  buildButton(),
                  buildButton(),
                ],
              ),
              new Row(
                children: [
                  buildButton(),
                  buildButton(),
                  buildButton(),
                  buildButton(),
                  buildButton(),
                ],
              ),
              new Row(
                children: [
                  buildButton(),
                  buildButton(),
                  buildButton(),
                  buildButton(),
                  buildButton(),
                ],
              ),
            ],
          )
        ],)
      )
    );
  }
}
